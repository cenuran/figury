package figury;

import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;

public class Trojkat extends Figura {

	public Trojkat(Graphics2D buf, int del, int w, int h) {
		super(buf, del, w, h);

		shape = new Polygon();

		((Polygon) shape).addPoint(0, 0);
		((Polygon) shape).addPoint(15, 30);
		((Polygon) shape).addPoint(30, 0);
		aft = new AffineTransform();
		area = new Area(shape);
	}

}