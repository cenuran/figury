package figury;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;

import java.awt.geom.Rectangle2D;

public class Kwadrat extends Figura {

	public Kwadrat(Graphics2D buf, int del, int w, int h) {
		super(buf, del, w, h);
		// TODO Auto-generated constructor stub
		shape = new Rectangle2D.Float(0, 0, 11, 11);
		aft = new AffineTransform();
		area = new Area(shape);
	}

}